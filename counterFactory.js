function counterFactory(){
    let counter = 1; 
    let result = {
        increment: function(){
            counter++; 
            return counter; 
        }, 
        decrement: function(){
            counter--; 
            return counter; 
        }
    }
    return result; 
}

module.exports = counterFactory; 