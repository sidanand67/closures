const cacheFunction = require('../cacheFunction');

const greetings = cacheFunction((name, msg) => {
    return name + ' ' + msg; 
}); 

console.log(greetings('sid','hello')); 
console.log(greetings("james", "hello")); 
console.log(greetings('sid', 'hello')); 