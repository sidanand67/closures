function cacheFunction(cb){
    let cache = {}; 
    return function(...args){
        let n = args; 
        if (cache[n] === undefined){
            cache[n] = cb(...args); 
        }
        return cache[n]; 
    }
}

module.exports = cacheFunction; 