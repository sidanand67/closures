function limitFunctionCallCount(cb, n){
    let functionCount = 0; 
    return function(...args){
        if (n && functionCount < n){
            cb(...args); 
            functionCount++; 
        }
        else {
            return null; 
        }
    }
}

module.exports = limitFunctionCallCount; 